/*
 * mouvement_serpent.h
 *
 *      Authors:	Nelson Fuerst
 *      			vincentnussbaumer
 */

#ifndef MOUVEMENT_SERPENT_H_
#define MOUVEMENT_SERPENT_H_

/*
 * D�marre la thread mouvement_serpent
 */
void start_thd_motor(void);

#endif /* MOUVEMENT_SERPENT_H_ */
