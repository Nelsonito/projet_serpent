#ifndef AUDIO_PROCESSING_H
#define AUDIO_PROCESSING_H

#include <math.h>

typedef enum {
	//2 times FFT_SIZE because these arrays contain complex numbers (real + imaginary)
	LEFT_CMPLX_INPUT = 0,
	RIGHT_CMPLX_INPUT,
	FRONT_CMPLX_INPUT,
	BACK_CMPLX_INPUT,
	//Arrays containing the computed magnitude of the complex numbers
	LEFT_OUTPUT,
	RIGHT_OUTPUT,
	FRONT_OUTPUT,
	BACK_OUTPUT
} BUFFER_NAME_t;


void processAudioData(int16_t *data, uint16_t num_samples);

/*
*	trouve la fr�quence principale et l'utilise pour choisir le pattern
*	que le moteur va devoir effectuer dans sa thread
*/
void detect_frequency(void);

/*
*	put the invoking thread into sleep until it can process the audio data
*/
void wait_send_to_computer(void);

/*
*	Returns the pointer to the BUFFER_NAME_t buffer asked
*/
float* get_audio_buffer_ptr(BUFFER_NAME_t name);

/*
 * Permet de r�cuperer la valeur du mode
 */
uint8_t get_mode(void);


#endif /* AUDIO_PROCESSING_H */
