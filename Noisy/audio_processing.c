/*
 * audio_processing.c
 *
 *  Derni�re modification : 14 mai 2021
 *
 *      Authors: 	MICRO-315
 *      			Nelson Fuerst
 *      			vincentnussbaumer
 *
 *     Fichier audio_processing du projet, repris du TP5 (TP son), il a �t� modifi�
 *     pour accueillir un syst�me de tri de fr�quence leur attribuant des formes g�om�triques
 *
 *     Utilise un micro (micRight)
 *
 */

#include "ch.h"
#include "hal.h"
#include <main.h>
#include <usbcfg.h>
#include <chprintf.h>

#include <audio/microphone.h>
#include <audio_processing.h>
#include <communications.h>
#include <fft.h>
#include <math.h>
#include <arm_math.h>

#define FFT_SIZE 		1024
#define NOISE_INTENSITY 10000

#define RIEN			0
#define LIGNE 			1
#define CERCLE_G 		2
#define CERCLE_D 		3
#define TRIANGLE 		4
#define ETOILE 			5


//fr�quences qui vont d�cider de quel mode le robot va fonctionner.
//on les choisit toutes en dessous de 350Hz pour les distinguer du bruit produit par le moteur en train de tourner.
#define MAX_LIGNE 		350
#define MIN_LIGNE 		320
#define MIN_CERCLE_G 	290
#define MIN_CERCLE_D 	260
#define MIN_TRIANGLE 	230
#define MIN_ETOILE 		200


// Appli(Hertz)		FFT

// 	211 Hertz	    500    son max pour que �a marche
//	1700 Hertz		400
//  3200 Hertz      300
//  4700 Hertz 		200	   1/3 du son max

//	2500-2900		320-350		LIGNE
//	2900-3400		290-320		CERCLE_G
//	3400-3900		260-290		CERCLE_D
//	3900-4300       230-260		TRIANGLE
//  4300-4700       200-230     ETOILE


//semaphore
static BSEMAPHORE_DECL(sendToComputer_sem, TRUE);

//2 times FFT_SIZE because these arrays contain complex numbers (real + imaginary)
static float micLeft_cmplx_input[2 * FFT_SIZE];
static float micRight_cmplx_input[2 * FFT_SIZE];
static float micFront_cmplx_input[2 * FFT_SIZE];
static float micBack_cmplx_input[2 * FFT_SIZE];
//Arrays containing the computed magnitude of the complex numbers
static float micLeft_output[FFT_SIZE];
static float micRight_output[FFT_SIZE];
static float micFront_output[FFT_SIZE];
static float micBack_output[FFT_SIZE];


static uint8_t mode = 0;


/*
*	Callback called when the demodulation of the four microphones is done.
*	We get 160 samples per mic every 10ms (16kHz)
*	
*	params :
*	int16_t *data			Buffer containing 4 times 160 samples. the samples are sorted by micro
*							so we have [micRight1, micLeft1, micBack1, micFront1, micRight2, etc...]
*	uint16_t num_samples	Tells how many data we get in total (should always be 640)
*/

void processAudioData(int16_t *data, uint16_t num_samples){
	/*
	*	We get 160 samples per mic every 10ms
	*	So we fill the samples buffers to reach
	*	1024 samples, then we compute the FFTs.
	*/
	static uint16_t compteur = 0;
	static uint8_t send = 0;
	uint16_t mic_samples = num_samples/4;
	for(int i = 0; i < mic_samples; i++){
		micRight_cmplx_input[compteur] = (float)data[4*i];
		//micLeft_cmplx_input[compteur] = (float)data[4*i+1];
		//micBack_cmplx_input[compteur] = (float)data[4*i+2];
		//micFront_cmplx_input[compteur] = (float)data[4*i+3];
		compteur++;
		micRight_cmplx_input[compteur] = 0;
		//micLeft_cmplx_input[compteur] = 0;
		//micBack_cmplx_input[compteur] = 0;
		//micFront_cmplx_input[compteur] = 0;
		compteur++;
		if(compteur>= 2*FFT_SIZE) break;
	}

	if(compteur>= 2*FFT_SIZE){
		compteur = 0;
		doFFT_optimized(FFT_SIZE, micRight_cmplx_input);
		//doFFT_optimized(FFT_SIZE, micLeft_cmplx_input);
		//doFFT_optimized(FFT_SIZE, micBack_cmplx_input);
		//doFFT_optimized(FFT_SIZE, micFront_cmplx_input);

		arm_cmplx_mag_f32(micRight_cmplx_input, micRight_output, FFT_SIZE);
		//arm_cmplx_mag_f32(micLeft_cmplx_input, micLeft_output, FFT_SIZE);
		//arm_cmplx_mag_f32(micBack_cmplx_input, micBack_output, FFT_SIZE);
		//arm_cmplx_mag_f32(micFront_cmplx_input, micFront_output, FFT_SIZE);

		if(send > 9){
			chBSemSignal(&sendToComputer_sem);
			SendFloatToComputer((BaseSequentialStream *) &SD3, micRight_output, FFT_SIZE);
			send = 0;
			detect_frequency();
			chThdSleepMilliseconds(100);
		}
		send++;
	}
}

void detect_frequency(){
	int micMain_frq = 0;
	float micHigh_out = 0;
	for(int i = 0; i<= FFT_SIZE/2; i++){
		if(micRight_output[i] > micHigh_out ){
			if((FFT_SIZE/2 - i) < MAX_LIGNE){ //pour ignorer le bruit g�n�r� par les moteurs (principalement au dessus de 350Hz)
				micHigh_out = micRight_output[i];
				micMain_frq = i;
			}
		}
	}
	micMain_frq =  FFT_SIZE/2 - micMain_frq;
	if(micHigh_out < NOISE_INTENSITY){
		micMain_frq = 0;
	}

	if(micMain_frq > MIN_LIGNE && micMain_frq < MAX_LIGNE){
		mode = LIGNE;
	}
	else if(micMain_frq > MIN_CERCLE_G && micMain_frq < MIN_LIGNE){
		mode = CERCLE_G;
	}
	else if(micMain_frq > MIN_CERCLE_D && micMain_frq < MIN_CERCLE_G){
		mode = CERCLE_D;
	}
	else if(micMain_frq > MIN_TRIANGLE && micMain_frq < MIN_CERCLE_D){
		mode = TRIANGLE;
	}
	else if(micMain_frq > MIN_ETOILE && micMain_frq < MIN_TRIANGLE){
		mode = ETOILE;
	}
	else{
		mode = RIEN;
	}
}

void wait_send_to_computer(void){
	chBSemWait(&sendToComputer_sem);
}

float* get_audio_buffer_ptr(BUFFER_NAME_t name){
	if(name == LEFT_CMPLX_INPUT){
		return micLeft_cmplx_input;
	}
	else if (name == RIGHT_CMPLX_INPUT){
		return micRight_cmplx_input;
	}
	else if (name == FRONT_CMPLX_INPUT){
		return micFront_cmplx_input;
	}
	else if (name == BACK_CMPLX_INPUT){
		return micBack_cmplx_input;
	}
	else if (name == LEFT_OUTPUT){
		return micLeft_output;
	}
	else if (name == RIGHT_OUTPUT){
		return micRight_output;
	}
	else if (name == FRONT_OUTPUT){
		return micFront_output;
	}
	else if (name == BACK_OUTPUT){
		return micBack_output;
	}
	else{
		return NULL;
	}
}

uint8_t get_mode(void){
	return mode;
}
