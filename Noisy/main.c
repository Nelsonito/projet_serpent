/*
 * main.c
 *
 *  Dernière modification : 14 mai 2021
 *
 *      Authors: 	MICRO-315
 *      			Nelson Fuerst
 *      			vincentnussbaumer
 *
 *     Main du projet, repris du TP5 (TP son), puis modifi� pour accueillir
 *     un module moteur et un capteur de proximit� TOF.
 *
 *     Le but de ce projet est de simuler un charmeur de serpent avec un robot
 *     qui se deplacera pour former des formes g�om�triques en fonction des fr�quences
 *     qu'il entendra.
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <stm32f4xx.h>


#include "ch.h"
#include "hal.h"
#include "memory_protection.h"
#include <usbcfg.h>
#include <main.h>
#include <chprintf.h>
#include <motors.h>
#include <audio/microphone.h>
#include <msgbus/messagebus.h>
#include <i2c_bus.h>
#include <sensors/imu.h>
#include <sensors/VL53L0X/VL53L0X.h>

#include <mouvement_serpent.h>
#include <audio_processing.h>
#include <fft.h>
#include <communications.h>
#include <arm_math.h>

//Message bus
messagebus_t bus;
MUTEX_DECL(bus_lock);
CONDVAR_DECL(bus_condvar);

#define NB_SAMPLES_OFFSET 200

static void serial_start(void)
{
	static SerialConfig ser_cfg = {
	    115200,
	    0,
	    0,
	    0,
	};

	sdStart(&SD3, &ser_cfg); // UART3.
}

static void timer12_start(void){
    //General Purpose Timer configuration   
    //timer 12 is a 16 bit timer so we can measure time
    //to about 65ms with a 1Mhz counter
    static const GPTConfig gpt12cfg = {
        1000000,        /* 1MHz timer clock in order to measure uS.*/
        NULL,           /* Timer callback.*/
        0,
        0
    };

    gptStart(&GPTD12, &gpt12cfg);
    //let the timer count to max value
    gptStartContinuous(&GPTD12, 0xFFFF);
}

int main(void)
{

    halInit();
    chSysInit();
    mpu_init();

    //starts the serial communication
    serial_start();
    //starts the USB communication
    usb_start();
    //starts timer 12
    timer12_start();
    //inits the motors
    motors_init();

    //imu

	messagebus_init(&bus, &bus_lock, &bus_condvar);
	chThdSleep(MS2ST(1000));
	i2c_start();
	imu_start();

	chThdSleep(MS2ST(1000));

	//on attend un moment que l'E-puck et ses modules soyent pr�ts
	chThdSleepMilliseconds(100);
	VL53L0X_start();
	//on attend que le capteur de distance TOF ait le temps de s'initialiser
	chThdSleepMilliseconds(500);

	mic_start(&processAudioData);
    start_thd_motor();

    /* Idle */
    while (1) {
    	chThdSleepMilliseconds(500);
    }
}

#define STACK_CHK_GUARD 0xe2dee396
uintptr_t __stack_chk_guard = STACK_CHK_GUARD;

void __stack_chk_fail(void)
{
    chSysHalt("Stack smashing detected");
}
