/*
 * mouvement_serpent.c
 *
 *  Dernière modification : 16 mai 2021
 *
 *      Authors:	Nelson Fuerst
 *      			vincentnussbaumer
 *
 *     Fichier mouvement_serpent du projet, g�re les d�placements moteurs du robot �
 *     l'aide d'une thread. R�cup�re le mode de mouvement d�termin� par la thread
 *     audio et utilise le gyroscope pour calculer les angles.
 *     G�re aussi les collisions � l'aide d'un capteur de distance TOF.
 *
 *     Utilise les modules motors.h, audio_processing.h, imu.h et VL53L0X.h
 *
 */

#include "ch.h"
#include "hal.h"
#include <main.h>
#include <usbcfg.h>
#include <chprintf.h>

#include <mouvement_serpent.h>
#include <motors.h>
#include <audio_processing.h>
#include <math.h>
#include <arm_math.h>

#include <sensors/imu.h>
#include <sensors/VL53L0X/VL53L0X.h>

//Collisions
#define DIST_MUR_MAX 	60	//mm
#define DIST_MUR_TOURNE 80

//Moteur
#define SPEEDY 			500
#define SPEED_ROT		218.98
//#define SPEED_ROT		225
//#define SPEED_ROT		216.9
#define STOP			0

//Modes d�placements
#define RIEN			0
#define LIGNE 			1
#define CERCLE_G 		2
#define CERCLE_D 		3
#define TRIANGLE 		4
#define ETOILE 			5
#define COLLISION		6
#define TOURNE			7

//Temps de d�placement
#define COUNT_TRIANGLE 	2000	//ms
#define COUNT_ETOILE 	2000
#define COUNT_ETOILE2  	4000 	//arbitraire
#define COUNT_ETOILE3  	6000

//Gyroscope
#define Z 				2

#define ANGLE_TRIANGLE  120	//degr�s
#define ANGLE_ETOILE1   144
#define ANGLE_ETOILE2   72
#define ANGLE_180    	180




/*
 * Initilaisation
 *
 * D�tecte les collisions de distance DIST_MUR_MAX (mm),
 * Recule un peu plus loin pour avoir de la marge, puis tourne de ANGLE_180 (degr�s)
 *
 * Retourne un uint8_t � r�attribuer aux modes de d�placements.
 */
uint8_t collision_imminente(uint8_t mode, uint8_t prev_mode, float z_angle);

/*
 * D�claraton de la thread mouvement_serpent, 128 semble �tre la m�moire minimum
 *
 * Dirige les mouvements du robot en formes g�om�triques, utilise :
 * get_mode() 				de audio_processing
 * get_gyro_rate()			de imu
 * collision_imminente() 	de mouvement_serpent
 * left_motor_set_speed()	de motor
 * right_motor_set_speed()	""""""""
 *
 * La thread semble imposante, mais "r�duire sa taille en ligne" en rempla�ant
 * les comportements par des fonction demanderait plus de m�moire alou�e (128 vs 256)
 */
static THD_WORKING_AREA(waThdMotor, 128);
static THD_FUNCTION(ThdMotor, arg) {
	chRegSetThreadName(__FUNCTION__);
	(void)arg;

	static systime_t prev_time = 0;
	static float z_angle = 0;	//angle z "parcouru" en degr�s
	static uint8_t prev_mode = 0;
	static uint16_t counter = 0;

	while(1){
		systime_t time;
		time = chVTGetSystemTime();
		uint8_t mode = get_mode();
		mode = collision_imminente(mode, prev_mode, z_angle);
		z_angle += (get_gyro_rate(Z)*180/M_PI) *(float)ST2MS(time-prev_time)/1000;

		//Reset des valeurs cl�s pour avoir le comportement d�sir� dans les d�placements
		if(mode != prev_mode){
			z_angle = 0;
			counter = 0;
			prev_mode = mode;
		}
		if (time > prev_time){
			//Branchement sur les modes de d�placements
			switch(mode){
				case(LIGNE):
					left_motor_set_speed(SPEEDY);
					right_motor_set_speed(SPEEDY);
					break;
				case(CERCLE_G):
					left_motor_set_speed(SPEEDY/2);
					right_motor_set_speed(SPEEDY);
					break;
				case(CERCLE_D):
					left_motor_set_speed(SPEEDY);
					right_motor_set_speed(SPEEDY/2);
					break;
				case(TRIANGLE):
					if(counter< COUNT_TRIANGLE){
						z_angle = 0;
						left_motor_set_speed(SPEEDY);
						right_motor_set_speed(SPEEDY);
						counter += ST2MS(time-prev_time);
						break;
					}
					else {
						if(z_angle < ANGLE_TRIANGLE && z_angle > (-ANGLE_TRIANGLE)){
							left_motor_set_speed(SPEED_ROT);
							right_motor_set_speed(-SPEED_ROT);
							break;
						}
						else{
							z_angle = 0;
							counter = 0;
							left_motor_set_speed(STOP);
							right_motor_set_speed(STOP);
							break;
						}
					}
				 case(ETOILE):
					if(counter < COUNT_ETOILE){
						left_motor_set_speed(SPEEDY);
						right_motor_set_speed(SPEEDY);
						counter += ST2MS(time-prev_time);
						z_angle = 0;
						break;
					}
					else if(counter < COUNT_ETOILE2){
						if(z_angle < ANGLE_ETOILE1  && z_angle > -ANGLE_ETOILE1 ){
							left_motor_set_speed(SPEED_ROT);
							right_motor_set_speed(-SPEED_ROT);
							break;
						}
						else{
							z_angle = 0;
							left_motor_set_speed(STOP);
							right_motor_set_speed(STOP);
							counter = COUNT_ETOILE2;
							break;
						}
					}
					else if (counter < COUNT_ETOILE3){
						left_motor_set_speed(SPEEDY);
						right_motor_set_speed(SPEEDY);
						counter += ST2MS(time-prev_time);
						z_angle = 0;
						break;
					}
					else{
						if(z_angle < ANGLE_ETOILE2  && z_angle > -ANGLE_ETOILE2 ){
							left_motor_set_speed(-SPEED_ROT);
							right_motor_set_speed(SPEED_ROT);
							break;
						}
						else{
							z_angle = 0;
							counter = 0;
							left_motor_set_speed(STOP);
							right_motor_set_speed(STOP);
							break;
						}
					}
				case(COLLISION):
					left_motor_set_speed(-SPEEDY);
					right_motor_set_speed(-SPEEDY);
					break;
				case(TOURNE):
					left_motor_set_speed(SPEED_ROT);
					right_motor_set_speed(-SPEED_ROT);
					break;
				case(RIEN):
				default :
					left_motor_set_speed(STOP);
					right_motor_set_speed(STOP);
					break;
			}
		}
		prev_time = time;
	}
}

void start_thd_motor(void){
	chThdCreateStatic(waThdMotor,sizeof(waThdMotor),NORMALPRIO-1,ThdMotor, NULL);
	//...(pointeur, taille, prio, ..., thread function, arg)
}

uint8_t collision_imminente(uint8_t mode, uint8_t prev_mode, float z_angle){
	if(VL53L0X_get_dist_mm() <= DIST_MUR_MAX){
		return COLLISION;
	}
	//Recule plus loin pour avoir de la marge
	else if(prev_mode == COLLISION && VL53L0X_get_dist_mm() <= DIST_MUR_TOURNE){
		return COLLISION;
	}
	else if(z_angle <= ANGLE_180 && z_angle >= (-ANGLE_180) && prev_mode >= COLLISION){
		return TOURNE;
	}
	else {
		return mode;
	}
}

